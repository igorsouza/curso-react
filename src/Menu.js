import React from 'react';
import {createDrawerNavigator} from 'react-navigation';

import Simples from './componentes/Simples';
import ParImpar from './componentes/ParImpar';
import { Inverter, Maiusculo, MegaSena } from './componentes/Multi';
import Contador from './componentes/Contador';
import Plataforma from './componentes/Plataforma';
import ValidarProps from './componentes/ValidarProps';
import Evento from './componentes/Evento';
import { Avo } from './componentes/ComunicacaoDireta';
import TextoSincronizado from './componentes/ComunicacaoIndireta';
import ListaFlex from './componentes/ListaFlex';

export default createDrawerNavigator({
    ListaFlex: {
        screen: ListaFlex,
        navigationOptions: { title: 'Lista de Alunos (Flex)'}
    },
    TextoSincronizado: {
        screen: () => <TextoSincronizado />,
        navigationOptions: { title: 'Texto Sincronizado'}
    },
    Avo: {
        screen: () => <Avo nome="Igor" sobrenome="Martins"/>
    },
    Evento: {
        screen: Evento
    },
    ValidarProps: {
        screen: () => <ValidarProps ano={19}/>
    },
    Plataforma: {
        screen: () => <Plataforma/>
    },
    Contador: {
        screen: () => <Contador numero={1}/>,
    },
    MegaSena: {
        screen: () => <MegaSena numeros={8}/>,
        navigationOptions: { title: 'Mega Sena'}
    },
    Inverter: {
        screen: () => <Inverter texto='Tela do inverter'/>
    },
    Maiusculo: {
        screen: () => <Maiusculo texto='Tela do maiúsculo'/>
    },
    ParImpar: {
        screen: () => <ParImpar numero={2}/>,
        navigationOptions: { title: 'Par & Impar'}
    },
    Simples: {
        screen: () => <Simples texto='Um simples texto'/>,
        navigationOptions: { title: 'Simples texto'}
    }
}, { drawerWidth: 300 })