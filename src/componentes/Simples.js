import React from 'react';
import { Text } from 'react-native';
import Padrao from '../estilos/Padrao';

/*
export default class Simples extends React.component {
    render() {
        return (
            <Text></Text>
        );
    }
}*/

/*
dois componentes usando array
export default props => [
    <Text key={1}>Texto1: {props.texto}</Text>,
    <Text key={2}>Texto2: {props.texto}</Text>
]
*/

/*
Dois componentes envolvidos por um View
export default props =>
    <View>
        <Text>Texto1: {props.texto}</Text>
        <Text>Texto2: {props.texto}</Text>
    </View>*/

export default function(props) {
    return <Text style={Padrao.ex}>{props.texto}</Text>
}