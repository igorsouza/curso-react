import React, { Component } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';


// 3 formas para setar o estado:
// Primeira: construtor com bind + chamada da funcao sem o ()
// Segunda: usando arrow function na declaracao da funcao e na hora de usar nao colocar o ()
// Terceira: declarar funcao normal mas usar arrow function na chamada da funcao

export default class Contador extends Component {
    state = {
        numero: this.props.numero
    }
    // construtor necessario para usar o bind
    constructor(props) {
        super(props);
        // usando bind na funcao pra setar o contexto this
        this.menosUm = this.menosUm.bind(this);
    }
    menosUm() {
        this.setState({ numero: this.state.numero - 1});
    }
    // usando funcoes normais
    maisUm(){
        this.setState({ numero: this.state.numero + 1});
    }
    //usando arrow function
    limpar = () => {
        this.setState({ numero: this.props.numero });
    }
    render() {
        return (
            <View>
                <Text style={{fontSize: 40}}>{this.state.numero}</Text>
                <TouchableHighlight 
                onPress={() => this.maisUm()}
                onLongPress={this.limpar}>
                    <Text>Incrementar/Zerar</Text>
                </TouchableHighlight>
                <TouchableHighlight 
                onPress={this.menosUm}
                onLongPress={this.limpar}>
                    <Text>Decrementar/Zerar</Text>
                </TouchableHighlight>
            </View>
        );
    }
}