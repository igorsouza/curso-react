import React from 'react';
import { ScrollView, View, FlatList, Text } from 'react-native';

const alunos =  [
    { id: 1, nome: 'João', nota: 9.4},
    { id: 2, nome: 'Maria', nota: 3.4},
    { id: 3, nome: 'José', nota: 8.2},
    { id: 4, nome: 'Ricardo', nota: 7.6},
    { id: 5, nome: 'Jonas', nota: 7.4},
    { id: 6, nome: 'Claudia', nota: 4.6},
    { id: 7, nome: 'Ana', nota: 1.2},
    { id: 8, nome: 'Manoel', nota: 5.4},
    { id: 9, nome: 'Igor', nota: 10.0},
    { id: 10, nome: 'Igor', nota: 10.0},
    { id: 11, nome: 'João', nota: 9.4},
    { id: 12, nome: 'Maria', nota: 3.4},
    { id: 13, nome: 'José', nota: 8.2},
    { id: 14, nome: 'Ricardo', nota: 7.6},
    { id: 15, nome: 'Jonas', nota: 7.4},
    { id: 16, nome: 'Claudia', nota: 4.6},
    { id: 17, nome: 'Ana', nota: 1.2},
    { id: 18, nome: 'Manoel', nota: 5.4},
    { id: 19, nome: 'Igor', nota: 10.0},
];

const itemEstilo = {
    paddingHorizontal: 15,
    height: 70,
    backgroundColor: '#ddd',
    borderWidth: 0.5,
    borderColor: '#222',

    /// *** Flex usado como coluna
    // alinha no centro
    // alignItems: 'center',

    // alinha na esquerda
    // alignItems: 'flex-start',

    // alinha na direita
    // alignItems: 'flex-end',

    // alinha o conteudo verticalmente no centro
    // justifyContent: 'center'

    // alinha o conteudo verticalmente no topo
    // justifyContent: 'flex-start',

    // alinha o conteudo verticalmente embaixo
    // justifyContent: 'flex-end',
    // alinha o conteudo usando espacoes em volta do texto
    // justifyContent: 'space-around',

    /// Flex usado como linha
    alignItems: 'center',
    flexDirection: 'row',
    // justifyContent: 'space-between'
    justifyContent: 'space-around'
};

export const Aluno = props => 
    <View style={itemEstilo}>
        <Text>Nome: {props.nome}</Text>
        <Text style={{fontWeight: 'bold'}}>Nota: {props.nota}</Text>
    </View>

export default props => {
    const renderItem = ({ item }) => {
        return <Aluno {...item}/>
    };
    return (
        <ScrollView>
            <FlatList data={alunos} renderItem={renderItem} keyExtractor={ (_, index) => index.toString()}/>
        </ScrollView>
    );
}
    