import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Simples from './componentes/Simples';
import ParImpar from './componentes/ParImpar';
import { Inverter, Maiusculo, MegaSena } from './componentes/Multi';
export default class App extends Component {
  render() {
    const texto = 'Flexível';
    return (
        <View style={styles.container}>
          <Simples texto={texto}/>
          <Inverter texto={texto}/>
          <Maiusculo texto={texto}/>
          <MegaSena numeros={6}/>
          <ParImpar numero={1}></ParImpar>
          <ParImpar numero={2}></ParImpar>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  f20: {
    fontSize: 40
  }
});